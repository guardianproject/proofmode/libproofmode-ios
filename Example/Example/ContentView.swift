//
//  ContentView.swift
//  Example
//
//  A small example showing how to use the ProofmodeLib library to generate proof for an photo.
//
//  It will generate proof for the picked photo and then display the photo hash and the names
//  of the generated proof files.
//
//  Created by N-Pex on 2023-02-02.
//

import SwiftUI
import Photos
import PhotosUI
import LibProofMode

struct ContentView: View {
    @State var showPhotoPicker: Bool = false
    @State var lastPhotoProofHash: String = ""
    @State var lastPhotoProofFiles: String = ""
    
    var body: some View {
        VStack {
            Button(action: {
                self.showPhotoPicker = true
            }) {
                Image(systemName: "photo")
                    .frame(width: 24, height: 24)
                Text("Select photo")
            }
            Text(lastPhotoProofHash)
                .font(.system(size: 8))
                .padding(EdgeInsets(top: 30, leading: 0, bottom: 0, trailing: 0))
            Text(lastPhotoProofFiles)
                .font(.system(size: 8))
                .padding(EdgeInsets(top: 30, leading: 0, bottom: 0, trailing: 0))
        }
        .padding()
        .sheet(isPresented: self.$showPhotoPicker, onDismiss: {
            showPhotoPicker = false
        }) {
            PhotoPicker { mediaItem in
                
                // Create proof options that control what to include in the proof, in addition to all default data.
                // Note: for some of these options you need to add permission entries into the info.plist.
                //
                let options = ProofGenerationOptions(showDeviceIds: false, showLocation: false, showMobileNetwork: true, notarizationProviders: [])
                
                // Call ProofmodeLib to generate the proof. Proof will be generated on file and a hash of the
                // media will be present in mediaItem.mediaItemHash upon successful proof generation.
                //
                Proof.shared.process(mediaItem: mediaItem, options: options, whenDone: { mediaItem in
                    if let hash = mediaItem.mediaItemHash {
                        self.lastPhotoProofHash = hash
                        self.lastPhotoProofFiles = ""
                        do {
                            // List all files in the proof folder for the given media item
                            //
                            if let proofDataFolder = Proof.shared.proofFolder(for: mediaItem) {
                                let fileManager = FileManager.default
                                let files = try fileManager.contentsOfDirectory(at: proofDataFolder, includingPropertiesForKeys: [])
                                for file in files {
                                    self.lastPhotoProofFiles += file.lastPathComponent + "\n"
                                }
                            }
                        } catch {
                            print("Failed to get proof files")
                            self.lastPhotoProofFiles = "Failed to get files"
                        }
                    } else {
                        self.lastPhotoProofHash = ""
                        self.lastPhotoProofFiles = ""
                    }
                })
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct PhotoPicker: UIViewControllerRepresentable {
    var didSelectPhoto: ((MediaItem) -> Void)
    
    func makeCoordinator() -> PhotoPicker.Coordinator {
        return Coordinator(parent: self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<PhotoPicker>) -> PHPickerViewController {
        let photoLibrary = PHPhotoLibrary.shared()
        var configuration = PHPickerConfiguration(photoLibrary: photoLibrary)
        configuration.selectionLimit = 1
        configuration.filter = PHPickerFilter.any(of: [PHPickerFilter.images])
        let picker = PHPickerViewController(configuration: configuration)
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
        
    }
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate, UINavigationControllerDelegate {
        let parent:PhotoPicker
        
        init(parent:PhotoPicker) {
            self.parent = parent
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            picker.dismiss(animated: true, completion: nil)
            if let pickedItem = results.first, let localAssetIdentifier = pickedItem.assetIdentifier {
                let fetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [localAssetIdentifier], options: nil)
                if fetchResult.count > 0 {
                    parent.didSelectPhoto(MediaItem(asset: fetchResult.object(at: 0)))
                }
            }
        }
    }
}
